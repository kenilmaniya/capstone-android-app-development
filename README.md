# Capstone Android App Development

This is a Capstone project for "Android App Development" from Vanderbilt University.

Images will be downloaded from : https://www.pexels.com/

The Downloaded picture will be shown to user and be able to store pictures locally.

There will be 2 activities : first is for downloading the pictures and  second will be used for viewing pictures.

Service will download the images in the background, and this service will broadcast when current picture download is completed.

Locale database will be present to store the favorite images.

Main components of the application are represented in the picture named Class Diagram :



